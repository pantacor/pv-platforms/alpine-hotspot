# Readme

## Testing the platform

### SSH

To log via SSH, you have to set the following user-meta key in Pantahub:

* key: pvr-sdk.authorized_keys
* value: <your-ssh-public-key>

After that, you can SSH your device:

port: 8022
user: root

### Access Point

SSID: apbootup
pass: pantacor
